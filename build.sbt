
//  define project common settings for current directory
lazy val commonSettings = Seq(
    organization := "org.bitbucket.franck44.expect",
    scalaVersion := "2.12.7"
)

libraryDependencies  ++=
    Seq (
        "org.scalatest" %% "scalatest" % "3.0.1" % "shippableci,test",
        "org.scalacheck" %% "scalacheck" % "1.13.4" % "shippableci,test",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
        "com.jsuereth" % "scala-arm_2.12" % "2.0",
        "ch.qos.logback" % "logback-classic" % "1.2.3"
        )

//      project settings
lazy val root = (project in file(".")).
    configs(ShippableCITest).
    settings(commonSettings: _*).
    settings(inConfig(ShippableCITest)(Defaults.testTasks): _*).
    settings(
      name := "expect-for-scala",
      scalacOptions  :=
          Seq (
              "-deprecation",
              "-feature",
              "-sourcepath", baseDirectory.value.getAbsolutePath,
              "-unchecked",
              "-Xlint",
              "-Xcheckinit"
          ),

            // Publishing

            publishTo := {
                // val nexus = "https://oss.sonatype.org/"
                Some(
                    if (version.value.trim.endsWith("SNAPSHOT"))
                        Opts.resolver.sonatypeSnapshots
                    else
                        Opts.resolver.sonatypeStaging
                )
            },
            publishConfiguration := publishConfiguration.value.withOverwrite(true),
            publishLocalConfiguration := publishLocalConfiguration.value.withOverwrite(true),
            publishMavenStyle := true,
            publishArtifact in Test := true,
            pomIncludeRepository := { x => false },
            pomExtra := (
                <url>https://bitbucket.org/franck44/expect-for-scala/</url>
                <licenses>
                    <license>
                        <name>LGPL, v. 3.0</name>
                        <url>https://www.gnu.org/licenses/lgpl-3.0.en.html</url>
                        <distribution>repo</distribution>
                    </license>
                </licenses>
                <scm>
                    <url>https://bitbucket.org/franck44/expect-for-scala</url>
                    <connection>scm:git:https://bitbucket.org/franck44/expect-for-scala</connection>
                </scm>
                <developers>
                    <developer>
                       <id>franck44</id>
                       <name>Franck Cassez</name>
                       <url>https://bitbucket.org/franck44</url>
                    </developer>
                </developers>
            )
          )

//  set profile for publishing
sonatypeProfileName  := "org.bitbucket.franck44"
// Add the default sonatype repository setting
publishTo := sonatypePublishTo.value



//  options for it
javaOptions in IntegrationTest ++= Seq("-Xss200M")

//  create a configuration for coverage test
lazy val ShippableCITest = config("shippableci") extend(Test)

// test options for generating formatted test results in IntegrationTest config for shippable
testOptions in ShippableCITest +=
    ((target in Test) map {
        t => Tests.Argument(TestFrameworks.ScalaTest, "-u", "%s" format (t / "../shippable/testresults"))
    }).value

fork in ShippableCITest := true

// scoverage package
coverageEnabled in ShippableCITest  := true
coverageMinimum in ShippableCITest := 80
coverageFailOnMinimum in ShippableCITest := false   //  false is safer as otherwise the build breaks
coverageHighlighting in ShippableCITest := true     //  enable highlighting of covered/non-covered
 // exclude some package from coverage
coverageExcludedPackages in ShippableCITest := ".*resources.*"
logBuffered in ShippableCITest := false
logLevel in ShippableCITest := Level.Info

scalaSource in ShippableCITest := baseDirectory.value / "src"
resourceDirectory in ShippableCITest := baseDirectory.value / "src/test/resources"

//  parallel execution
parallelExecution in Test := true
parallelExecution in ShippableCITest := true

logLevel in ThisBuild := Level.Info

//  sbt shell prompt
shellPrompt in ThisBuild := {
    state =>
        Project.extract(state).currentRef.project + " [" + name.value + "] " + version.value +
            " " + scalaVersion.value + "> "
}

// ScalariForm for automatic formatting at compile time

import scalariform.formatter.preferences._
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys

ScalariformKeys.preferences := ScalariformKeys.preferences.value
    .setPreference (AlignSingleLineCaseStatements, true)
    .setPreference (IndentSpaces, 4)
    .setPreference (SpaceBeforeColon, true)
    .setPreference (SpaceInsideBrackets, true)
    .setPreference (SpaceInsideParentheses, true)
    .setPreference (SpacesWithinPatternBinders, true)
    .setPreference (SpacesAroundMultiImports, true)
    .setPreference (SpacesAroundMultiImports, true)
    .setPreference (PreserveSpaceBeforeArguments, true)
    .setPreference (SpacesWithinPatternBinders, true)
    .setPreference (RewriteArrowSymbols, true)
    .setPreference (AlignParameters, true)
    .setPreference(AlignArguments, true)
    .setPreference(DanglingCloseParenthesis, Preserve)

headerMappings := headerMappings.value + (HeaderFileType.scala -> HeaderCommentStyle.cStyleBlockComment)

headerLicense := Some(HeaderLicense.Custom(
    """| This file is part of Expect-for-Scala.
       |
       | Copyright (C) 2015-2018 Franck Cassez.
       |
       | Expect-for-Scala is free software: you can redistribute it and/or modify
       | it under the terms of the GNU Lesser General Public License as published
       | by the Free Software Foundation, either version 3 of the License, or (at
       | your option) any later version.
       |
       | Expect-for-Scala is distributed in the hope that it will be  useful, but
       | WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
       | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
       |
       | See the GNU Lesser General Public License for  more details.
       |
       | You should have received a copy of the GNU Lesser General Public License
       | along with Expect-for-Scala. (See files COPYING and COPYING.LESSER.)  If
       | not, see  <http://www.gnu.org/licenses/>.
       |""".stripMargin
    )
)

excludeFilter.in(headerSources) := HiddenFileFilter || "src/generated/**"

//  note: use headerCreate in sbt to generate the headers
//  headerCheck to check which files need new headers
//  headers generations can also be automated at compile time
//  to generate headers for test files, test:headerCreate
