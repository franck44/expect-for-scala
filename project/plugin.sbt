
addSbtPlugin ("org.scalariform" % "sbt-scalariform" % "1.8.2")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")

addSbtPlugin("de.heikoseeberger" % "sbt-header" % "5.0.0")

//  import the sbt-sonatype plugin
addSbtPlugin("org.xerial.sbt" % "sbt-sonatype" % "2.3")
addSbtPlugin("com.jsuereth" % "sbt-pgp" % "1.1.1")

// add sbt-ensime to your build or in global.sbt
addSbtPlugin("org.ensime" % "sbt-ensime" % "2.5.1")

