/*
 *  This file is part of Expect-for-Scala.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Expect-for-Scala is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  Expect-for-Scala is distributed in the hope that it will be  useful, but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Expect-for-Scala. (See files COPYING and COPYING.LESSER.)  If
 *  not, see  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.expect
package tests

import org.scalatest.prop.TableDrivenPropertyChecks
import scala.util.matching.Regex

object ImplicitResources {

    import resource._

    /**
     * Provide an implicit to close unused resources
     */
    implicit object ExpectResource extends Resource[ Expect ] {

        /**
         * Kills the process spawned by ProcessBuilder
         */
        def close( p : Expect ) : Unit = p.destroy()

    }
}

import ImplicitResources._
import resource._
import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{ Try, Failure }
import scala.concurrent.duration._

/**
 * Tests that the correct exceptions are trigered
 */
class ExceptionTests extends FunSuite with Matchers {

    override def suiteName = "Exception tests"

    def getProjectRootDirectory = new java.io.File( "." ).getCanonicalPath

    //  name of bash script, arguments
    val ( procName, procArgs ) =
        (
            getProjectRootDirectory + "/src/test/resources/test-terminal.sh",
            List( "<EOC>" )
        )

    test( "Send read::3, timeout 2, should timeout and raise a TimeoutException" ) {

        for ( e ← managed ( Expect( procName, procArgs ) ) ) {
            import scala.concurrent.duration._

            e.send( "read::3\n" )
            val response : Try[ String ] = e.expect( """<EOC>""".r, 2.seconds )

            response.isFailure shouldBe true
            an[ scala.concurrent.TimeoutException ] should be thrownBy response.get
        }
    }

    test( "Send read::2, timeout 2 but wrong prompt, should raise an TimeoutException" ) {

        for ( e ← managed ( Expect( procName, procArgs ) ) ) {
            import scala.concurrent.duration._

            e.send( "read::3\n" )
            val response : Try[ String ] = e.expect( """fin""".r, 2.seconds )

            response.isFailure shouldBe true
            an[ java.util.concurrent.TimeoutException ] should be thrownBy response.get
        }
    }
}

/**
 * Simple test with a send and reponse and send and timeout
 */
class OneSendReceiveExpectTests extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Simple expec tests  (takes 3 seconds)"

    def getProjectRootDirectory = new java.io.File( "." ).getCanonicalPath

    //  name of bash script, arguments
    val ( procName, procArgs ) =
        (
            //  this test-terminal  echoes its input a after k secnds when sent a::k
            //  the prompt is "<EOC>"
            getProjectRootDirectory + "/src/test/resources/test-terminal.sh",
            List( "<EOC>" )
        )

    test( "Send read::1, timeout 2, should be a Success" ) {

        //  get a managed Expect object
        for ( e ← managed ( Expect( procName, procArgs ) ) ) {

            e.send( "read::1\n" )
            val response : Try[ String ] = e.expect( """<EOC>""".r, 2.seconds )

            //  extract text within whitespaces
            val re = """(\s*)(\w*)(\s*)""".r
            val re( x, y, z ) = response.get
            y shouldBe "read"
        }
    }

    import scala.concurrent.TimeoutException

    test( "Send read::2, timeout 1, should be a Failure" ) {

        //  get a managed Expect object
        for ( e ← managed ( Expect( procName, procArgs ) ) ) {

            e.send( "read::2\n" )
            val response : Try[ String ] = e.expect( """<EOC>""".r, 1.seconds )

            response should matchPattern { case Failure( _ ) ⇒ }

            the[ TimeoutException ] thrownBy {
                response.get
            } should have message "Futures timed out after [1 second]"
        }
    }
}

/**
 * Multiple send/receive
 */
class MultipleSendReceiveExpectTests extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Multiple send and receive expec tests  (takes 10 seconds)"

    //  path to find executable (shells)
    def getProjectRootDirectory = new java.io.File( "." ).getCanonicalPath

    //  name of bash script, arguments
    val ( procName, procArgs ) =
        (
            getProjectRootDirectory + "/src/test/resources/test2-terminal.sh",
            List()
        )

    //  format: OFF
    val expectTable = Table[ String, Int, Regex, String ](
        ( "send"                             , "timeout" , "prompt"     , "response" )             ,
        ( "read::1 writer::1"                , 3         , """er\s""".r , "readwrit" )             ,
        ( "writer::1 reading::1"             , 3         , """ng""".r   , "writerreadi" )          ,
        ( "writer::1 reading::1 talking>::1" , 4         , """>""".r    , "writerreadingtalking" )
    )
    //  format: ON

    //  get a managed Expect object

    for ( e ← managed ( Expect( procName, procArgs ) ) ) {
        forAll ( expectTable ) { ( s : String, t : Int, pr : Regex, r : String ) ⇒
            e.send( s + "\n" )
            val response : Try[ String ] = e.expect( pr, t.seconds )

            //  check response limited to sequence of characters
            ( response.get.filter( ch ⇒ ch >= 'A' && ch <= 'z' ) ) shouldBe r
        }
    }
}
